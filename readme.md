# Plantillas para presentaciones con LaTeX

## diapositivas
  * [darkConsole](#darkconsole)
  * [UDL](#UDL)
  * [UFRN](#UFRN)
  * [unicamp](#unicamp)
  * [QUT](#QUT)

## libros
  * [legrand orange](#legrand_orange)
  * [ElegantBook](#ElegantBook) 

## papers
  * [rtai](#rtai) 
  * [paper](#paper)
  * [g3](#g3)
  * [blackwell](#blackwell)

## posters
  * [muw](#muw)
  * [ermac](#ermac)

## tesis
  * [ucp](#ucp)
  * [upm](#upm)
  * [tfm](#tfm)

## Instrucciones especiales
  * [codigo ajustado](#codigo_ajustado)

# Instrucciones de uso de las plantillas
## darkconsole
Se tiene que estar dentro del [directorio](darkConsole) de la plantilla 
y correr el comando 
```bash
$ pandoc --to=beamer --include-in-header=style.tex --toc --standalone --output=../example.pdf ../test.md
```
## UDL

Se tiene que estar dentro del [directorio](UDL) de la plantilla 
y correr el comando 
```bash
$ pandoc --to=beamer --include-in-header=style.tex --output=../example.pdf ../test.md
```

## UFRN

Se tiene que estar dentro del [directorio](UFRN) de la plantilla 
y correr el comando 
```bash
$ pandoc --to=beamer --include-in-header=style.tex --output=../example.pdf  --toc ../test.md
```

## unicamp

Se tiene que estar dentro del [directorio](unicamp) de la plantilla 
y correr el comando 
```bash
$ pandoc --to=beamer --include-in-header=style.tex --output=../example.pdf ../test.md
```

## QUT
Se tiene que estar dentro del [directorio](QUT) de la plantilla 
```bash
$ pandoc --to=beamer --include-in-header=style.tex --output=../example.pdf ../test.md
```

## legrand_orange
Se tiene que estar dentro del [directorio](legrand_orange) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=book  --variable fontsize=11pt,fleqn --top-level-division=part --template=template.latex --reference-location=section --preserve-tabs --output=../example.pdf ../test.md
```
* NOTA: Esta plantilla YA funciona correctamente!
## ElegantBook
Se tiene que estar dentro del [directorio](ElegantBook) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --standalone --variable documentclass=elegantbook --variable abstract= --toc --output=../example.pdf ../test.md
```

## rtai
Se tiene que estar dentro del [directorio](rtai) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=abntex2 --output=../example.pdf ../test.md
```

## paper
Se tiene que estar dentro del [directorio](paper) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=article --variable fontsize=10pt,twocolumn --output=../example.pdf ../test.md
```

## G3
Se tiene que estar dentro del [directorio](g3) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --include-before=style2.tex --variable documentclass=gsag3jnl --variable fontsize=9pt,twocolumn,twoside,lineno --template=template.latex --output=../example.pdf ../test.md
```

## Blackwell
Se tiene que estar dentro del [directorio](blackwell) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=tellus --variable fontsize=11pt,useAMS,usenatbib --template=template.latex --output=../example.pdf ../test.md
```

## Muw
Se tiene que estar dentro del [directorio](muw) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --include-after-body=style_2.tex --variable documentclass=beamer --variable columnwidth=0.3\paperwidth --pdf-engine=xelatex --output=../example.pdf ../test.md
```
* Funciona pero no muestra texto
## ermac
Se tiene que estar dentro del [directorio](ermac) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --include-after-body=style_2.tex --variable documentclass=beamer --variable columnwidth=0.3\paperwidth --pdf-engine=xelatex --output=../example.pdf ../test.md
```
* Funciona pero no muestra texto
## ucp
Se tiene que estar dentro del [directorio](ucp) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=scrreprt --variable paper=A4,12pt,twoside,openright,abstract --toc --listings --output=../readme.pdf ../readme.md
```
## upm
Se tiene que estar dentro del [directorio](upm) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=upm-report --variable fontsize=12pt,school=etsisi,type=pers,degree=61TI,authorsex=m,directorsex=m --top-level-division=chapter --template=template.latex --pdf-engine=xelatex --output=../example.pdf ../test.md
```
* REQUIERE TIPO DE FUENTE PROPIETARIA NO DISPONIBLE EN TODOS LOS SISTEMAS

## tfm
Se tiene que estar dentro del [directorio](tfm) de la plantilla 
```bash
$ pandoc --include-in-header=style.tex --variable documentclass=scrreprt --template=template.latex --pdf-engine=xelatex --output=../example.pdf ../test.md
```

## codigo_ajustado
Para que el codigo se pueda mostrar correctamente se tiene que agregar la siguiente 
instrucción al final del style.tex
```latex
\usepackage{listings}
% Colores para resaltar codigo
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
% Definicio de estilo para el codigo fuente que se cita
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
}
\lstset{style=mystyle}
```

Adicionalmente hay que agregar esta bandera a el comando para invocar pandoc 
`--listings`
