---
author: Carlos R. Tlahuel Pérez
title: EGC Sistemas Expertos
institute: Posgrado en Ciencia e Ingeniería de la Computación
date: March 22, 2005
abstract: |
  owever, this problem could be resolved by monitoring the effective sample size and tuning the factorfor covariance inflation. In this paper, the proposed hybrid algorithm is introduced, and its performanceis evaluated through experiments with non-Gaussian observations

  de algo con otro parrafo
acknowledgement: |
  Algunos agradecimientos this problem could be resolved by monitoring the effective sample size and tuning the factorfor covariance inflation. In this paper, the proposed hybrid algorithm is introduced, and its performanceis evaluated through experiments with non-Gaussian observations

toc-title: titulo de la tabla de contenidos
keywords: [keyword1, keyword2]
university: Universidad Nacional Autónoma de México
chapter: Introduccion y esas cosas
---
# In the morning
## Getting up
- Turn off alarm
- Get out of bed

## Breakfast
- Eat eggs
- Drink coffee

# In the evening
## Dinner
- Eat spaghetti
- Drink wine

## Going to sleep
- Get in bed
- Count sheep

<!-- pandoc --to=beamer --include-in-header=style.tex --standalone --output=../test.pdf ../test.md -->